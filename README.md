# Befunge Number Generator
random number generator written befunge-93

## Run
I'd reccomend using [fungide](https://amicloud.github.io/fungide/) to run it if you just want to try it quickly. Paste the `random-number.bf` file, click 'To Interpreter Mode', and 'Crawl' or 'Play'. **Do not click 'Run'**, because the number generator goes on forever. If you want something local, there's a [Befunge-93 interpreter](https://github.com/programble/befungee) written in Python.

## Befunge Resources
A good place to start is the [Esolangs wiki page](https://esolangs.org/wiki/Befunge); it has a good overview and some other resources. The [Befunge-93 documentiation](https://catseye.tc/projects/befunge93/doc/befunge93.html) is good if you want to take a deeper look at Befunge.

## Todo
- [ ] Make as small as possible (in terms of area, that is)
